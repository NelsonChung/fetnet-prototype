import React from 'react';

import tab1 from '../resources/images/tab-1.png';
import tab2 from '../resources/images/tab-2.png';
import tab3 from '../resources/images/tab-3.png';
import tab4 from '../resources/images/tab-4.png';
import tab1Active from '../resources/images/tab-1-active.png';
import tab2Active from '../resources/images/EBU-tab-icon-2.png';
import tab3Active from '../resources/images/EBU-tab-icon-3.png';
import tab4Active from '../resources/images/EBU-tab-icon-4.png';

import EBUBanner1 from '../Component/EBUBanner1';
import EBUBanner2 from '../Component/EBUBanner2';
import EBUBanner3 from '../Component/EBUBanner3';
import EBUBanner4 from '../Component/EBUBanner4';

const EbuB = (props) => {
    const [tabs] = React.useState([
        {text: '微型店家', img: tab1, active: tab1Active, content: EBUBanner1},
        {text: '中型企業', img: tab2, active: tab2Active, content: EBUBanner2},
        {text: '大型企業', img: tab3, active: tab3Active, content: EBUBanner3},
        {text: '公部門', img: tab4, active: tab4Active, content: EBUBanner4}
    ]);
    const [tabIndex, setTabIndex] = React.useState(0);
    
    const tabChange = (index) => {
        setTabIndex(index)
    }

    return (
        <main>
            <ul className="tab page-tab">
                {
                    tabs.map((tab, index) => (
                        <li className={index===tabIndex ? 'active' : ''}>
                            <a onClick={e => tabChange(index)}>
                                <div className="image">
                                    <img src={index===tabIndex ? tab.active : tab.img} height="28" />
                                </div>
                                <div>{tab.text}</div>
                            </a>
                        </li>
                    ))
                }
            </ul>
            <div className="ebu-tab-content">
                {
                    tabs.map((tab, pidex) => (
                        <div className={`page ${tabIndex===pidex ? 'current' : ''}`} key={`pagetab-content-${pidex}`}>
                            <tab.content isCurrent={tabIndex===pidex} />
                        </div>
                    ))
                }
            </div>
        </main>
    )
}


export default EbuB;