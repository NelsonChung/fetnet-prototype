import React from 'react';
import {useHistory, useLocation} from 'react-router-dom';

import survey1 from '../resources/images/ebu-1-survey.jpg'
import survey2 from '../resources/images/ebu-2-survey.jpg'

const Survey = (props) => {
    const location = useLocation()
    let lo = location.pathname.split(/\//g)
    lo = lo.reverse()

    console.log(lo)
    return (
        <main>
            <img src={lo[0]==='1' ? survey1 :  survey2} width="100%" />
        </main>
    );
}

export default Survey;