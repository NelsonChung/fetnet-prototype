import React from 'react';
import VideoPage from '../resources/images/video-page.jpg';

const Video = (props) => {
    return (
        <main>
            <img src={VideoPage} width="100%" />
        </main>
    )
}

export default Video;