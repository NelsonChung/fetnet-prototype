import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import VisibilitySensor from 'react-visibility-sensor';
import { Parallax } from 'react-scroll-parallax';

import tab1 from '../resources/images/tab-1-1.png';
import tab1Active from '../resources/images/tab-1-1-active.png';
import tab2 from '../resources/images/tab-1-2.png';
import tab2Active from '../resources/images/CBU-tab-icon-2@2x.png';
import tab3 from '../resources/images/tab-1-3.png';
import tab3Active from '../resources/images/tab-1-3-active.png';
import tab4 from '../resources/images/tab-1-4.png';
import tab4Active from '../resources/images/CBU-tab-icon-4@2x.png';
import tab5 from '../resources/images/tab-1-5.png';
import tab5Active from '../resources/images/tab-1-5-active.png';
import bgColorTail from "../resources/images/bg-color-tail.png";
import bgLife from "../resources/images/bg-life.png";
import lifeCards from "../resources/images/life-cards.png";
import lifeItems from "../resources/images/life-items.png";
import bottomTrangle from "../resources/images/bottom-trangle.png";
import youLikeBg from "../resources/images/bg-you-like.png";
import youLikeCards from "../resources/images/you-like-cards.png";
import bgNews from "../resources/images/bg-news.png";
import newsTrangle from "../resources/images/news-bottom-trangle.png";
import newsCards from "../resources/images/news-cards.png";

import bubble1 from "../resources/images/bubble-1.png";
import bubble2 from "../resources/images/bubble-2.png";
import bubble3 from "../resources/images/bubble-3.png"; 
import bubble4 from "../resources/images/bubble-4.png"; 
import find from "../resources/images/find.png"; 

import SwipeableViews from 'react-swipeable-views';
import CBUBanner1 from "../Component/CBUBanner1";
import CBUBanner2 from "../Component/CBUBanner2";
import CBUBanner3 from "../Component/CBUBanner3";
import CBUBanner4 from "../Component/CBUBanner4";
import CBUBanner5 from "../Component/CBUBanner5";
import arrow from '../resources/images/chevron-right-thin.png';

const IndexB = (props) => {
    const [tabs] = React.useState([
        {img: tab1, active: tab1Active, text: '手機', content: CBUBanner1},
        {img: tab2, active: tab2Active, text: '資費', content: CBUBanner2},
        {img: tab3, active: tab3Active, text: 'Yo! 活', content: CBUBanner3},
        {img: tab4, active: tab4Active, text: '優惠', content: CBUBanner4},
        {img: tab5, active: tab5Active, text: '服務', content: CBUBanner5}
    ]);
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';

    const [tabIndex, setTabIndex] = React.useState(0);
    const [show, setShow] = React.useState(false);
    const [cardShow, setCardShow] = React.useState(false);
    const [lifeShow, setLifeShow] = React.useState(false);
    const [newsShow, setNewsShow] = React.useState(false);
    const [isScrolling, setScrolling] = React.useState(false);
    const [scrollPos, setScrollPos] = React.useState(0);
    
    // history.listen(listener => {
    //     window.removeEventListener('scroll', setPromotion);
    // });

    const switchTab = (index) => {
        setTabIndex(index)
    }

    const visibleChange = (visible) => {
        if (visible && !window.loading) {
            setShow(true)
        }
    }

    const CardChange = (visible) => {
        if (visible && !window.loading) {
            setCardShow(true)
        }
    }

    const lifeCardChange = (visible) => {
        if (visible && !window.loading) {
            setLifeShow(true)
        }
    }

    const newsCardChange = (visible) => {
        if (visible && !window.loading) {
            setNewsShow(true)
        }
    }

    window.onscroll = (e) => {
        // debugger
        setPromotion(e)
    }

    const gotoVideo = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/video`)
        }, 300)
    }

    const setPromotion = (e) => {
        if( !Array.from(document.querySelectorAll('section.promotion')).length) return
        setScrolling(true)
        let promo = Array.from(document.querySelectorAll('section.promotion'))[tabIndex];
        let offset = promo.offsetTop - window.innerHeight * 0.66
        let deltaY = window.scrollY > scrollPos
        // debugger
        if (window.scrollY >= offset && promo.clientHeight + (window.scrollY - offset)<500 && deltaY) {
            promo.classList.add('expand');
        } 
        
        if (window.scrollY < promo.offsetTop && promo.clientHeight - (offset - window.scrollY) > 0 && !deltaY) {
            promo.classList.remove('expand');
        }

        setScrollPos(window.scrollY)
    }

    return (
        <main>
            <ul className="tab page-tab">
                {
                    tabs.map((tab, index) => (
                        <li className={tabIndex === index ? 'active' : ''} key={`pagetab-${index}`}>
                            <a onClick={e => switchTab(index)}>
                                <div className="image"><img src={tabIndex === index ? tab.active : tab.img} height="32" alt={`tab-img-${index}`} /></div>
                                <div>{tab.text}</div>
                            </a>
                        </li>
                    ))
                }
            </ul>
            <SwipeableViews index={tabIndex} onChangeIndex={switchTab}>
                {
                    tabs.map((tab, pidex) => (
                        <div className={`page ${tabIndex===pidex ? 'current' : ''}`} key={`pagetab-content-${pidex}`}>
                            <tab.content isScrolling={isScrolling} index={pidex} />
                        </div>
                    ))
                }
            </SwipeableViews>
            <section className="bubble">
                <Parallax y={[10, -10]} className="bg-img bg-color-tail"><img src={bgColorTail} width="405" alt='bubble-bg-1' /></Parallax>
                <Parallax y={[20, 0]} className="bg-trangle"><img src={bottomTrangle} alt='bubble-bg-2'/></Parallax>
                <div className="container">
                    <h3>早安！雅婷</h3>
                    <VisibilitySensor onChange={visibleChange} offset={50} partialVisibility={true}> 
                        <div>
                            <img src={bubble1} className={`is-animate ${show ? 'is-in':''}`} 
                            style={{transitionDelay: '.5s', marginLeft: '10px', width: '323px'}} />

                            <img src={bubble2} className={`is-animate ${show ? 'is-in':''}`} 
                            style={{transitionDelay: '.8s', marginLeft: '10px', marginTop: '-45px', width: '323px'}} />
                            
                            <div className={`scroller is-animate ${show ? 'is-in':''}`} 
                            style={{transitionDelay: '1s', paddingLeft: 0, marginTop: '-45px'}}>
                                <img src={bubble3} height="334" style={{marginLeft: '25px'}} />
                            </div>
                            <div style={{marginTop: '-50px', marginLeft: '9px'}}>
                                <img src={bubble4} height="30" width="375" />
                            </div>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section className="you-may-like">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={youLikeBg} width="100%" alt='like-bg-1'/>
                </Parallax>
                <div className="container">
                    <h3>你可能會有興趣</h3>
                    <VisibilitySensor onChange={CardChange} partialVisibility={true}> 
                        <div className={`scroller is-animate ${cardShow ? 'is-in':''}`}>
                            <img src={youLikeCards} height="440" alt='like-card-1'/>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section className="you-life">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={bgLife} width="100%" alt='life-bg-1' />
                </Parallax>
                <div className="container">
                    <h3>Yo! 活與你共度日常</h3>
                    <VisibilitySensor onChange={lifeCardChange} partialVisibility={true}> 
                        <div>
                            <div className={`is-animate ${lifeShow ? 'is-in':''}`} style={{textAlign: 'center', margin: '0 -15px'}}>
                                <button onClick={gotoVideo}></button>
                                <img src={lifeCards} width="375" alt='life-cards'/>
                            </div>
                            <div 
                                className={`scroller is-animate ${lifeShow ? 'is-in':''}`} 
                                style={{ paddingLeft: '15px'}}
                            >
                                <img src={lifeItems} height="93" alt='life-items'/>
                            </div>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section style={{paddingBottom: '60px'}}>
                <Parallax y={[30, 0]} className="bg-img is-bottom">
                    <img src={bgNews} width="100%" alt='news-bg-1'/>
                </Parallax>
                <Parallax y={[40, 0]} className="bg-img trangle">
                    <img src={newsTrangle} width="100%" alt='news-bg-2'/>
                </Parallax>
                <div className="container">
                    <div className="header">
                        <div className="title">
                            <h3>來看新消息</h3>
                        </div>
                        <div className='action'>
                            看更多 
                            <img src={arrow} height="14" />
                        </div>
                    </div>
                </div>
                <VisibilitySensor onChange={newsCardChange} partialVisibility={true}> 
                    <div className={`is-animate ${newsShow ? 'is-in':''}`}>
                        <img src={newsCards} height="240" alt='news-cards'/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="img-sec">
                <img src={find} height="294" alt='find-img'/>
            </section>
        </main>
    )
}


export default IndexB;