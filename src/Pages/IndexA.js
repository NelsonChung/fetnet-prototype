import React from 'react';
import VisibilitySensor from 'react-visibility-sensor';

import buttonDot from '../resources/images/button-dot.png'
import buttonDotActive from '../resources/images/button-dot-active.png'

import Phone1 from '../resources/images/asus-phone.png'
import Phone2 from '../resources/images/phone-pixel-4.png'
import Phone3 from '../resources/images/galaxy-note10.png'
import Phone4 from '../resources/images/huawei-y9.png'
import youLikeCards from '../resources/images/A-you-like-cards.png'
import Video from '../resources/images/A-video.png'
import ShortCut1 from '../resources/images/shortcut-1.png'
import ShortCut2 from '../resources/images/shortcut-2.png'
import ShortCut3 from '../resources/images/shortcut-3.png'
import ShortCut4 from '../resources/images/shortcut-4.png'
import Video1 from '../resources/images/A-video-1.png'
import Video2 from '../resources/images/A-video-2.png'
import Video3 from '../resources/images/A-video-3.png'
import Video4 from '../resources/images/A-video-4.png'
import Video5 from '../resources/images/A-video-5.png'
import Video6 from '../resources/images/A-video-6.png'
import Video7 from '../resources/images/A-video-7.png'
import Video8 from '../resources/images/A-video-8.png'
import sitemap from '../resources/images/A-sitemap.png'
import newsContent from '../resources/images/A-news.png'
import shortcuts from '../resources/images/shortcuts.png'
import ActionMenuImg from '../resources/images/actionMenu.png'
import closeIconA from '../resources/images/closeIconA.png';
import arrow from '../resources/images/chevron-right-thin.png';
import findCardBg1 from "../resources/images/find-card-bg-1.png";
import findCardBg2 from "../resources/images/find-card-bg-2.png";
import findCardBg3 from "../resources/images/find-card-bg-3.png";
import findCardBg4 from "../resources/images/find-card-bg-4.png";
import memberCards from "../resources/images/member-cards.png";


import Marquee from "../Component/Marquee";
import BBanner1 from "../Component/BBanner1";
import BBanner2 from "../Component/BBanner2";
import BBanner3 from "../Component/BBanner3";
import BBanner4 from "../Component/BBanner4";
import BBanner5 from "../Component/BBanner5";

const IndexA = (props) => {
    const findtabs = [
        {text: '找手機'},
        {text: '找資費'},
        {text: '找服務'}
    ]
    const lifetabs = [
        {text: '玩家聯盟時刻'},
        {text: '我的追劇人生'},
        {text: '就是要宅在家'},
        {text: '小資旅遊GO!'},
        {text: '跑山環島騎到飽'}
    ]
    const [findTab, setFindTab] = React.useState(0)
    const [lifeTab, setLifeTab] = React.useState(0)
    const [findPhone, setFindPhone] = React.useState(false)
    const [youlike, setYoulike] = React.useState(false)
    const [video, setVideo] = React.useState(false)
    const [news, setNews] = React.useState(false)
    const [actionMenu, setActionMenu] = React.useState(false)
    const [shorcutOpen, setShorcutOpen] = React.useState(false)
    const [shorcutClose, setShorcutClose] = React.useState(false)

    const [curtTab, setCurtTab] = React.useState(0)
    const videoItems = [
        {height: 136, img: Video1, text: '傲骨賢妻'}, 
        {height: 136, img: Video2, text: '18歲的瞬間'}, 
        {height: 136, img: Video3, text: '你也是人類嗎？'}, 
        {height: 136, img: Video4, text: '大力女子'},
        {height: 136, img: Video5, text: '他的私生活'}, 
        {height: 136, img: Video6, text: '阿爾罕布拉宮的秘密'}, 
        {height: 136, img: Video7, text: '雲畫的月光'}, 
        {height: 136, img: Video8, text: '觸及真心'},
    ];
    const tabs = [
        {text: '手機'},
        {text: '資費'},
        {text: 'Yo! 活'},
        {text: '網路限定'},
        {text: '優惠'}
    ]

    const changeTab = index => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            setCurtTab(index)
            document.getElementsByTagName('html')[0].classList.remove('fade-out');
        }, 500)
    }

    const openShortcut = (e) => {
        setShorcutOpen(true)
    }
    const closeShortcut = (e) => {
        setShorcutClose(true)

        setTimeout(() => {
            setShorcutOpen(false)
            setShorcutClose(false)
        }, 1000)
    }

    const actionMenuChange = (e) => {
        if(!actionMenu) 
            e.target.focus()
        setActionMenu(!actionMenu)
    }
    
    const newsChange = isVisible => {
        if (isVisible)
            setNews(isVisible)
    }
    const videoChange = isVisible => {
        if (isVisible)
            setVideo(isVisible)
    }
    const youlikeChange = isVisible => {
        if (isVisible)
            setYoulike(isVisible)
    }
    const FindPhoneChange = isVisible => {
        if (isVisible)
            setFindPhone(isVisible)
    }
    const changeFindTab = index => {
        setFindTab(index)
    }
    const changeLifeTab = index => {
        setLifeTab(index)
    }

    const banner = () => {
        let ba = null
        switch(curtTab) {
            case 1:
                ba = <BBanner2/>
                break;
            case 2:
                ba = <BBanner3/>
                break;
            case 3:
                ba = <BBanner4/>
                break;
            case 4:
                ba = <BBanner5/>
                break;
            default:
                ba = <BBanner1/>
        }

        return ba;
    }
    return (
        <main>
            <div className='page-tabs'>
                <ul className="tab page-tab slash-tab">
                    {
                        tabs.map((tab, index) => (
                            <li className={ curtTab === index ? `active` : ''} key={`page-tab-${index}`}>
                                <a onClick={e => changeTab(index)}>{tab.text}</a>
                            </li>
                        ))
                    }
                </ul>
            </div>
            
            {
                banner()
            }
            
            <section className="find-phone">
                <VisibilitySensor onChange={FindPhoneChange} partialVisibility={true}> 
                    <div className={`container is-animate ${findPhone ? 'is-in' : ''}`}>
                        <div className="header">
                            <div className="title">
                                <ul className="tab slash-tab">
                                    {
                                        findtabs.map((tab, index) => (
                                            <li className={ findTab === index ? `active` : ''} key={`find-tab-${index}`}>
                                                <a onClick={e => changeFindTab(index)}>{tab.text}</a>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                            <div className="action">
                                <label onClick={actionMenuChange} onBlur={actionMenuChange}>
                                    <img src={actionMenu ? buttonDotActive : buttonDot} height="36" />
                                    <div className={`menu ${actionMenu ? 'is-open' : ''}`}>
                                        <img src={ActionMenuImg} width="205" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div className="cards">
                            <div className={`card square is-animate ${findPhone ? 'is-in' : ''}`} 
                            style={{
                                backgroundColor: '#505C6F', 
                                backgroundImage: `url(${findCardBg1})`,
                                transitionDelay: '.5s'
                            }}>
                                <img src={Phone1} height='158' alt="ASUS Zenfone 6 128G" />
                                <div className='caption'>
                                    ASUS<br/>Zenfone 6 128G
                                </div>
                            </div>
                            <div className={`card is-animate ${findPhone ? 'is-in' : ''}`} 
                            style={{
                                backgroundColor: '#98A4A7', 
                                backgroundImage: `url(${findCardBg2})`,
                                transitionDelay: '.8s'
                            }}>
                                <img src={Phone2} height='168' alt='Pixel4 128G' />
                                <div className='caption'>
                                    Google<br/>Pixel4 128G
                                </div>
                            </div>
                            <div className={`card is-animate ${findPhone ? 'is-in' : ''}`} 
                            style={{
                                backgroundColor: '#B19687', 
                                backgroundImage: `url(${findCardBg3})`,
                                transitionDelay: '1.1s'
                            }}>
                                <img src={Phone3} height='153' alt='galaxy-note10' />
                                <div className='caption'>
                                    Samsung<br/>
                                    Galaxy Note 10<br/>
                                    256G 
                                </div>
                            </div>
                            <div className={`card square is-animate ${findPhone ? 'is-in' : ''}`} 
                            style={{
                                backgroundColor: '#623E3E', 
                                backgroundImage: `url(${findCardBg4})`,
                                transitionDelay: '1.4s'
                            }}>
                                <img src={Phone4} height='174' alt='HUAWEI Y9 Prime 2019' />
                                <div className='caption'>
                                    HUAWEI <br/>Y9 Prime 2019
                                </div>
                            </div>
                        </div>
                        <div className="labels">
                            <span className='label'>iPhone</span>
                            <span className='label'>平價入門</span>
                            <span className='label'>iPhone11</span>
                            <span className='label'>影像更出色</span>
                            <span className='label'>Android</span>
                            <a className='more'>看更多<img src={arrow} height="14" /></a>
                        </div>
                    </div>
                </VisibilitySensor>
            </section>
            <section className='you-like'>
                <div className='container'>
                    <h3>你可能會喜歡</h3>
                </div>
                <VisibilitySensor onChange={youlikeChange} partialVisibility={true}> 
                    <div className={`scroller is-animate ${youlike ? 'is-in' : ''}`}>
                        <img src={youLikeCards} alt='you-like' height='512'/>
                    </div>
                </VisibilitySensor>
            </section>
            <section>
                <div className="container">
                    <div className='tab-container'>
                        <ul className="tab slash-tab">
                            {
                                lifetabs.map((tab, index) => (
                                    <li className={ lifeTab === index ? `active` : ''} key={`life-tab-${index}`}>
                                        <a onClick={e => changeLifeTab(index)}>{tab.text}</a>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
                <VisibilitySensor onChange={videoChange} partialVisibility={true}> 
                    <div className={`videoCarousel is-animate ${video ? 'is-in' : ''}`}>
                        <div className='video'>
                            <img src={Video} width="100%" />
                        </div>
                        <Marquee loopData={videoItems}/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className='member'>
                <div className="container">
                    <img src={memberCards} width="100%" />
                </div>
            </section>
            <section className='news'>
                <div className='container'>
                    <div className='header'>
                        <div className='title'>
                            <h3>最新消息</h3>
                        </div>
                        <div className='action'>
                            <a>看更多<img src={arrow} height="14" /></a>
                        </div>
                    </div>
                    <VisibilitySensor onChange={newsChange} partialVisibility={true}> 
                        <div className={`is-animate ${news ? 'is-in' : ''}`}>
                            <img src={newsContent} width='100%' />
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section style={{padding: 0}}>
                <img src={sitemap} width='100%' />
            </section>
            <div className='shortcuts-trigger'>
                <img src={shortcuts} height="52" onClick={openShortcut} />
            </div>
            <div className={`shortcuts ${shorcutOpen ? 'is-open' : ''} ${shorcutClose ? 'is-closing' : ''}`}>
                <div className='content'>
                    <div className='item'>
                        <img src={ShortCut1} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut2} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut3} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut4} height="44" />
                    </div>
                    <div className="close">
                        <img src={closeIconA} height="24" alt='close' onClick={closeShortcut} />
                    </div>
                </div>
            </div>
        </main>
    )
}

export default IndexA;