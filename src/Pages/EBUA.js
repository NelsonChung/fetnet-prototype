import React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import Slider from "react-slick";
import { Typography } from '@material-ui/core';

import serviceTags from "../resources/images/A-service-tags.png";
import service from "../resources/images/A-ebu-service.png";
import newsContent from "../resources/images/A-ebu-news.png";
import bgCircle2 from '../resources/images/bgCircle2.png'
import skillCircle from '../resources/images/skillCircle.png'
import BSkillCard from '../resources/images/B-skill-cards.png'
import ShortCut1 from '../resources/images/shortcut-1.png'
import ShortCut2 from '../resources/images/shortcut-2.png'
import ShortCut3 from '../resources/images/shortcut-3.png'
import ShortCut4 from '../resources/images/shortcut-4.png'
import shortcuts from '../resources/images/shortcuts.png'
import closeIconA from '../resources/images/closeIconA.png';
import arrow from '../resources/images/chevron-right-thin.png';

import likeImg from "../resources/images/A-ebu-like.png";
import demoShopBg from "../resources/images/ADemoBg.png";
import QuoteEnd from "../resources/images/quote-end.png";
import QuoteBegin from "../resources/images/quote-begin.png";
import shopLogo1 from "../resources/images/shop-1-logo.png";
import shopLogo2 from "../resources/images/shop-2-logo.png";
import shopLogo3 from "../resources/images/shop-3-logo.png";
import shopLogo4 from "../resources/images/shop-4-logo.png";
import pormotionMail from "../resources/images/a-pormotion-mail.png";
import skillBg from "../resources/images/EBU-skill-bg.png";
import EbuCardBg1 from "../resources/images/ebu-card-bg-1.png";
import EbuCardBg2 from "../resources/images/ebu-card-bg-2.png";
import MemberCard from "../resources/images/member-ebu-cards.png";
import EbuCardBg4 from "../resources/images/ebu-card-bg-4.png";
import EbuCardBg5 from "../resources/images/ebu-card-bg-5.png";

import BBanner1 from "../Component/AEBUBanner1";
import BBanner2 from "../Component/AEBUBanner1";
import BBanner3 from "../Component/AEBUBanner1";
import BBanner4 from "../Component/AEBUBanner1";
import SwipeableViews from 'react-swipeable-views';

import EBULink from '../resources/images/B-ebu-link.png';

const EbuA = (props) => {
    const [shopTab, setShopTab] = React.useState(0)
    const [curtTab, setCurtTab] = React.useState(0)
    const [skillTab, setSkillTab] = React.useState(0)
    const [news, setNews] = React.useState(false)
    const [shorcutOpen, setShorcutOpen] = React.useState(false)
    const [shorcutClose, setShorcutClose] = React.useState(false)
    const [shopShow, setShopShow] = React.useState(false)
    const [ulike, setUlike] = React.useState(false)
    const [skill, setSkill] = React.useState(false)
    const [member, setMember] = React.useState(false)

    const settings = {
        infinite: true,
        speed: 1000,
        afterChange: current => setShopTab(current),
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };

    const tabs = [
        {text: '微型店家'},
        {text: '中型企業'},
        {text: '大型企業'},
        {text: '公部門'}
    ]

    const skilltabs = [
        {text: '餐飲業'},
        {text: '零售業'},
        {text: '美容業'},
        {text: '全部'}
    ]

    const [demoShops] = React.useState([
        {
            background: demoShopBg,
            logo: shopLogo1,
            name: 'The 41 Bistro',
            meta: '肆拾壹號英式小酒館',
            content: 'Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。'
        },
        {
            background: demoShopBg,
            logo: shopLogo2,
            name: '花草慢時光',
            meta: '',
            content: 'Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。'
        },
        {
            background: demoShopBg,
            logo: shopLogo3,
            name: 'TH Korea Salon',
            meta: 'Find The Life Kitchen',
            content: 'Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。' 
        },
        {
            background: demoShopBg,
            logo: shopLogo4,
            name: '楓露',
            meta: 'Find The Life Kitchen',
            content: 'Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。'
        }
    ]);

    const memberChange = isVisible => {
        if (isVisible)
            setMember(isVisible)
    }

    const skillChange = isVisible => {
        if (isVisible)
            setSkill(isVisible)
    }

    const ulikeChange = isVisible => {
        if (isVisible)
            setUlike(isVisible)
    }

    const changeSkillTab = index => {
        setSkillTab(index)
    }
    const changeTab = index => {
        setCurtTab(index)
    }
    const changeShopTab = index => {
        setShopTab(index)
    }

    const openShortcut = (e) => {
        setShorcutOpen(true)
    }

    const closeShortcut = (e) => {
        setShorcutClose(true)

        setTimeout(() => {
            setShorcutOpen(false)
            setShorcutClose(false)
        }, 1000)
    }

    const newsChange = isVisible => {
        if (isVisible)
            setNews(isVisible)
    }

    const shopChange = isVisible => {
        if (isVisible)
            setShopShow(isVisible)
    }

    return (
        <main>
            <div className='page-tabs'>
                <ul className="tab slash-tab">
                    {
                        tabs.map((tab, index) => (
                            <li className={ curtTab === index ? `active` : ''} key={`page-tab-${index}`}>
                                <a onClick={e => changeTab(index)}>{tab.text}</a>
                            </li>
                        ))
                    }
                </ul>
            </div>
            <SwipeableViews index={curtTab} onChangeIndex={changeTab}>
                <BBanner1 current={curtTab} onUpdate={changeTab} />
                <BBanner2 current={curtTab} onUpdate={changeTab} />
                <BBanner3 current={curtTab} onUpdate={changeTab} />
                <BBanner4 current={curtTab} onUpdate={changeTab} />
            </SwipeableViews>
            <section className='u-like'>
                <div className='container'>
                    <h3>您可能會喜歡</h3>
                </div>
                <VisibilitySensor onChange={ulikeChange} partialVisibility={true}> 
                    <div className={`scroller is-animate ${ulike ? 'is-in' : ''}`}>
                        <img src={likeImg} height='512' />
                    </div>
                </VisibilitySensor>
                <div style={{padding: '20px', textAlign: 'center'}}>
                    <img src={EBULink} height="14" />
                </div>
            </section>
            <section className='skill'>
                <VisibilitySensor onChange={skillChange} partialVisibility={true}> 
                    <div className={`container is-animate ${skill ? 'is-in' : ''}`}>
                        <h3>開業技法新知</h3>
                        <ul className="tab slash-tab auto-width">
                            {
                                skilltabs.map((tab, index) => (
                                    <li className={ skillTab === index ? `active` : ''} key={`skill-tab-${index}`}>
                                        <a onClick={e => changeSkillTab(index)}>{tab.text}</a>
                                    </li>
                                ))
                            }
                        </ul>
                        <img src={BSkillCard} width='100%' style={{marginBottom: "30px"}} />
                        <img src={pormotionMail} width='100%'/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className='demo-shop' style={{paddingBottom: 0}}>
                <div className='container'>
                    <div className="header">
                        <div className='title'>
                            <h3>選擇我們的頭家</h3>
                        </div>
                        <div className='action'>
                            <a>更多成功案例<img src={arrow} height="14" /></a>
                        </div>
                    </div>
                    <VisibilitySensor onChange={shopChange} partialVisibility={true}> 
                        <div className={`is-animate ${shopShow ? 'is-in' : ''}`}>
                            <ul className="tab demo-shop">
                                {
                                    demoShops.map((shop, index) => (
                                        <li className={shopTab===index ? 'active' : ''} key={`shop-${index}`}>
                                            <a onClick={e => changeShopTab(index)}><img src={shop.logo} width="50" /></a>
                                        </li>
                                    ))
                                }
                            </ul>
                            <Slider autoplay={true} {...settings} arrows={true} className="demo-shop-carousel is-main" dots={false}>
                                {
                                    demoShops.map((item, idx) => (
                                        <div key={`carousel-${idx}`} align="center" className='demo-shop-carousel-item'>
                                            <div style={{
                                                backgroundImage: `url(${item.background})`
                                            }}>
                                                <div className='caption'>
                                                    <Typography variant="body1" component="p" className="shop-name">
                                                        {item.name}  {item.meta!== '' ? `- ${item.meta}` : ''}
                                                    </Typography>
                                                    
                                                    <Typography variant="body1" component="p" className="shop-descrption">
                                                        <img src={QuoteBegin} height="13" className="quote begin" />
                                                        {item.content}
                                                        <img src={QuoteEnd} height="13" className="quote end" />
                                                    </Typography>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </Slider>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section className='member'>
                <div className="container">
                    <VisibilitySensor onChange={memberChange} partialVisibility={true}> 
                        <img src={MemberCard} width="345" />
                    </VisibilitySensor>
                </div>
            </section>
            <section className='news'>
                <div className='container'>
                    <div className='header'>
                        <div className='title'>
                            <h3>消息與公吿</h3>
                        </div>
                        <div className='action'>
                            <a>看更多<img src={arrow} height="14" /></a>
                        </div>
                    </div>
                    <VisibilitySensor onChange={newsChange} partialVisibility={true}> 
                        <div className={`is-animate ${news ? 'is-in' : ''}`}>
                            <img src={newsContent} width='100%' />
                            <hr/>
                            <img src={service} height='89' />
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section style={{padding: 0, lineHeight: 0}}>
                <img src={serviceTags} width="375" />
            </section>
            <div className='shortcuts-trigger'>
                <img src={shortcuts} height="52" onClick={openShortcut} />
            </div>
            <div className={`shortcuts ${shorcutOpen ? 'is-open' : ''} ${shorcutClose ? 'is-closing' : ''}`}>
                <div className='content'>
                    <div className='item'>
                        <img src={ShortCut1} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut2} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut3} height="44" />
                    </div>
                    <div className='item'>
                        <img src={ShortCut4} height="44" />
                    </div>
                    <div className="close">
                        <img src={closeIconA} height="24" alt='close' onClick={closeShortcut} />
                    </div>
                </div>
            </div>
        </main>
    )
}

export default EbuA;