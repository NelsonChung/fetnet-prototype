import React from 'react';

import Slider from "react-slick";
import { Typography } from '@material-ui/core';

import toFooter from '../resources/images/toFooter.png';
import bar from '../resources/images/ebu-plan-bar.png';
import tab1 from '../resources/images/rest-tab-1.png';
import tab2 from '../resources/images/rest-tab-2.png';
import tab3 from '../resources/images/rest-tab-3.png';
import tab1Active from '../resources/images/rest-tab-1-active.png';
import tab2Active from '../resources/images/rest-tab-2-active.png';
import tab3Active from '../resources/images/rest-tab-3-active.png';
import content2 from '../resources/images/rest-content-2.png';
import content3 from '../resources/images/rest-content-3.png';
import contentTitle from '../resources/images/tab-content-title.png';
import banner1 from '../resources/images/rest-banner-1.png';
import banner2 from '../resources/images/rest-banner-2.png';
import banner3 from '../resources/images/rest-banner-3.png';
import SliderTitle from '../resources/images/rest-slider-title.jpg';
import RestCard1 from '../resources/images/rest-cards-1.png';
import RestCard2 from '../resources/images/rest-cards-2.png';
import RestCard3 from '../resources/images/rest-cards-3.png';
import RestCard4 from '../resources/images/rest-cards-4.png';

import demoShopBg from "../resources/images/demo-shop-bg.png";
import shopLogo1White from "../resources/images/shop-1-logo-white.png";
import shopLogo2White from "../resources/images/shop-2-logo-white.png";
import shopLogo3White from "../resources/images/shop-3-logo-white.png";
import shopLogo4White from "../resources/images/shop-4-logo-white.png";
import quote from "../resources/images/A-ebu-quote.png";
import barBg from '../resources/images/topbar-bg.png';

const EBUPlan = (props) => {
    const [tabIndex, setTabIndex] = React.useState(0)
    const carousel = React.useRef(null)
    const [mapIndex, setMapIndex] = React.useState(0)
    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true,
        arrows: false
    };

    const [demoShops] = React.useState([
        {
            background: demoShopBg,
            logo: shopLogo1White,
            name: '花草慢時光',
            meta: '',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo2White,
            name: 'TH Korea Salon',
            meta: 'Find The Life Kitchen',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」' 
        },
        {
            background: demoShopBg,
            logo: shopLogo3White,
            name: '楓露',
            meta: 'Find The Life Kitchen',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo4White,
            name: 'The 41 Bistro',
            meta: '肆拾壹號英式小酒館',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
    ]);

    const switchTab = index => {
        setTabIndex(index)
        carousel.current.slickGoTo(index)
    }

    const switchMap = index => {
        setMapIndex(index)
    }

    window.onscroll = (e) => {
        if(!document.querySelector('.section-topbar')) return;
        if (window.scrollY > 458) {
            document.querySelector('.section-topbar').classList.add('is-show')
        } else {
            document.querySelector('.section-topbar').classList.remove('is-show')
        }
    }

    return (
        <main>
            <div className="section-topbar" style={{
                backgroundImage: `url(${barBg})`,
                backgroundRepeat: 'repeat-x',
                backgroundSize: '5px 53px'
            }}>
                <img src={bar} height="45" />
            </div>
            <Slider ref={carousel} {...settings} className="is-main" autoplay={false}>
                <div>
                    <img src={banner1} width="100%" />
                </div>
                <div>
                    <img src={banner2} width="100%" />
                </div>
                <div>
                    <img src={banner3} width="100%"/>
                </div>
            </Slider>
            <div className="rest-tab">
                <div className={`tab-item ${tabIndex === 0 ? 'is-active' : ''}`} onClick={e => switchTab(0)}>
                    <img src={tabIndex === 0 ? tab1Active : tab1} height="80" />
                </div>
                <div className={`tab-item ${tabIndex === 1 ? 'is-active' : ''}`} onClick={e => switchTab(1)}>
                    <img src={tabIndex === 1 ? tab2Active : tab2} height="80" />
                </div>
                <div className={`tab-item ${tabIndex === 2 ? 'is-active' : ''}`} onClick={e => switchTab(2)}>
                    <img src={tabIndex === 2 ? tab3Active : tab3} height="80" />
                </div>
            </div>
            <div style={{backgroundColor: '#FFF'}} className={`rest-tab-content ${tabIndex===0 ? 'is-active' : ''}`}>
                <img src={contentTitle} width="100%" />
                <div className="tab-buttons">
                    <div onClick={e => switchMap(0)} className={`tab-item ${mapIndex===0 ? 'is-active' : ''}`}>用餐區</div>
                    <div onClick={e => switchMap(1)} className={`tab-item ${mapIndex===1 ? 'is-active' : ''}`}>櫃檯區</div>
                    <div onClick={e => switchMap(2)} className={`tab-item ${mapIndex===2 ? 'is-active' : ''}`}>廚房區</div>
                    <div onClick={e => switchMap(3)} className={`tab-item ${mapIndex===3 ? 'is-active' : ''}`}>辦公區</div>
                </div>
                <div className={`rest-tab-content ${mapIndex===0 ? 'is-active' : ''}`}>
                    <img src={RestCard1} width="100%" />
                </div>
                <div className={`rest-tab-content ${mapIndex===1 ? 'is-active' : ''}`}>
                    <img src={RestCard2} width="100%" />
                </div>
                <div className={`rest-tab-content ${mapIndex===2 ? 'is-active' : ''}`}>
                    <img src={RestCard3} width="100%" />
                </div>
                <div className={`rest-tab-content ${mapIndex===3 ? 'is-active' : ''}`}>
                    <img src={RestCard4} width="100%" />
                </div>
            </div>
            <div style={{backgroundColor: '#FFF'}} className={`rest-tab-content ${tabIndex===1 ? 'is-active' : ''}`}>
                <img src={content2} width="100%" />
            </div>
            <div style={{backgroundColor: '#FFF'}} className={`rest-tab-content ${tabIndex===2 ? 'is-active' : ''}`}>
                <img src={content3} width="100%" />
            </div>
            <img src={SliderTitle} width="100%"/>
            <Slider autoplay={true} {...settings} arrows={true} className="demo-shop-carousel is-main" dots={true}>
                {
                    demoShops.map((item, idx) => (
                        <div key={`carousel-${idx}`} align="center" className='demo-shop-carousel-item'>
                            <div style={{
                                backgroundImage: `url(${item.background})`
                            }}>
                                <img src={item.logo} alt={item.name} height='80' />
                                <img src={quote} className="quote" height='19' />
                                <Typography variant="body1" component="p">
                                    {item.content}
                                </Typography>
                                <Typography variant="body1" component="p" className="shop-name">
                                    {item.name}  {item.meta!== '' ? `- ${item.meta}` : ''}
                                </Typography>
                            </div>
                        </div>
                    ))
                }
            </Slider>
            <div style={{lineHeight: 0}}>
                <img src={toFooter} width="100%" />
            </div>
        </main>
    )
}

export default EBUPlan;