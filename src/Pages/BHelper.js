import React from 'react';

import HelperCenter from '../resources/images/helper-center.png'

const Helper = (props) => {
    return (
        <main>
            <img src={HelperCenter} width="100%" />
        </main>
    );
}

export default Helper;