import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import Slider from 'react-slick';
import LifePage from '../resources/images/life-page.png';
import Bar from '../resources/images/yo-bar.png';
import Banner1 from '../resources/images/yo-banner-1.png';
import Banner2 from '../resources/images/yo-banner-2.png';
import Banner3 from '../resources/images/yo-banner-3.png';
import Items from '../resources/images/life-items.png';
import barBg from '../resources/images/topbar-bg.png';

const YoLife = (props) => {
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';

    const settings = {
        infinite: true,
        speed: 1000,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };

    const goVideoPage = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/video`)
        }, 300)
    }
    return (
        <main>
            <div className="section-topbar no-button" style={{
                background: `url(${barBg}) repeat-x`,
                backgroundSize: '5px 53px'
            }}>
                <img src={Bar} height="45" style={{position: 'relative', zIndex: 1}} />
            </div>
            <section style={{paddingBottom: '20px'}}>
                <Slider {...settings} className="yo-life">
                    <div>
                        <img src={Banner1} height="314" />
                    </div>
                    <div>
                        <img src={Banner2} height="314" />
                    </div>
                    <div>
                        <img src={Banner3} height="314" />
                    </div>
                </Slider>
                <h2 className="text-center" style={{marginTop: '35px', marginBottom: '20px'}}>早安，今天需要什麼服務？</h2>
                <div className="scroller" style={{margin: '0'}}>
                    <img src={Items} height="100"/>
                </div>
            </section>
            <div style={{
                position: 'relative'
            }}>
                <div style={{
                    position: 'absolute',
                    display: 'block',
                    width: '334px',
                    right: '0',
                    left: '0',
                    top: '190px',
                    margin: 'auto',
                    height: '185px'
                }}
                onClick={goVideoPage}
                ></div>
                <img src={LifePage} width="100%" />
            </div>
        </main>
    )
}

export default YoLife;