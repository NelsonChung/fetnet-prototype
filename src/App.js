import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ParallaxProvider } from 'react-scroll-parallax';
// import Page from 'react-page-loading'

import Loader from './Component/Loader'
import Header from './Component/Header'
import Footer from './Component/Footer'
import IndexB from './Pages/IndexB'
import IndexA from './Pages/IndexA'
import EbuB from './Pages/EBU'
import EbuA from './Pages/EBUA'
import Helper from './Pages/BHelper'
import LifePage from './Pages/YoLife'
import VideoPage from './Pages/VideoPage'
import EBUPlan from './Pages/EBUPlan'
import Survey from './Pages/Survey'

const App = () => {
  return (
    <Router>
      <Header/>
      <ParallaxProvider>
        <Switch>
          <Route path="/" exact component={IndexA}  />
          <Route path="/conceptB/index" component={IndexA} />
          <Route path="/conceptB/ebu" component={EbuA} />
          <Route path="/conceptA/index" component={IndexB} />
          <Route path="/conceptA/ebu" component={EbuB}  />
          <Route path="/conceptA/helper-center" component={Helper}  />
          <Route path="/conceptA/yo-life" component={LifePage}  />
          <Route path="/conceptA/video" component={VideoPage}  />
          <Route path="/conceptA/ebu-plan" component={EBUPlan}  />
          <Route path="/conceptA/survey/*" component={Survey}  />
        </Switch>
      </ParallaxProvider>
      <Footer/>
      <Loader/>
    </Router>
  );
}

export default App;
