import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import ebuBg from '../resources/images/ebu-banner-2.png';
import VisibilitySensor from 'react-visibility-sensor';
import { Parallax } from 'react-scroll-parallax';
import { Typography } from '@material-ui/core';
import Slider from "react-slick";
import Marquee from "../Component/Marquee";
import arrow from '../resources/images/chevron-right-thin.png';

import ebuArrLeft from '../resources/images/ebu-arrow-left.png';
import ebuArrRight from '../resources/images/ebu-arrow-right.png';
import ebuService from "../resources/images/ebu-service.png";
import newsTrangle from "../resources/images/news-bottom-trangle.png";
import newsCards from "../resources/images/ebu-news-card.png";
import ebuTags from "../resources/images/ebu-tags-2.png";
import demoShopBg from "../resources/images/demo-shop-bg.png";
import shopLogo1 from "../resources/images/shop-logo-2-1.png";
import shopLogo2 from "../resources/images/shop-logo-2-2.png";
import shopLogo3 from "../resources/images/shop-logo-2-3.png";
import shopLogo4 from "../resources/images/shop-logo-2-4.png";
import shopLogo5 from "../resources/images/shop-logo-2-5.png";
import shopLogo6 from "../resources/images/shop-logo-2-6.png";
import shopLogo1White from "../resources/images/shop-logo-2-1-white.png";
import shopLogo2White from "../resources/images/shop-logo-2-2-white.png";
import shopLogo3White from "../resources/images/shop-logo-2-3-white.png";
import shopLogo4White from "../resources/images/shop-logo-2-4-white.png";
import shopLogo5White from "../resources/images/shop-logo-2-5-white.png";
import shopLogo6White from "../resources/images/shop-logo-2-6-white.png";

import shopPromo from "../resources/images/shop-promotion-2.png";
import quote from "../resources/images/A-ebu-quote.png";

import OpenSkill from "../resources/images/open-skill-2.png";
import EbuPromotion from "../resources/images/ebu-promotion-2.png";

import youLikeCards from "../resources/images/ebu2-you-like-cards.png";
import EBUMore1 from "../resources/images/ebu-more-2.png";
import sitemap from '../resources/images/ebu-sitemap-2.png';


const EBUBanner1 = (props) => {
    const tabs = [
        {text: '全部'},
        {text: '遠傳大人物'},
        {text: '醫療商機'},
        {text: 'AIOT'}
    ];
    const [cardShow, setCardShow] = React.useState(false);
    const [lifeShow, setLifeShow] = React.useState(false);

    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';

    const [tabIndex, setTabIndex] = React.useState(0);
    const [shopShow, setShopShow] = React.useState(false);
    const [newsShow, setNewsShow] = React.useState(false);
    const [promoShow, setPromoShow] = React.useState(false);
    const [marqueeShow, setMarqueeShow] = React.useState(false);
    const [sitemapShow, setSitemapShow] = React.useState(false);
    const [tagShow, setTagShow] = React.useState(false);

    const [demoShops] = React.useState([
        {
            background: demoShopBg,
            logo: shopLogo1White,
            name: '台北寒舍艾美酒店',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo2White,
            name: '王品集團營運支援中心',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」' 
        },
        {
            background: demoShopBg,
            logo: shopLogo3White,
            name: '台灣大車隊',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo4White,
            name: 'Mazeda',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo5White,
            name: '金色三麥',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo6White,
            name: 'Live ABC',
            meta: '鄭榮輝 副總經理',
            content: '「去機房化之後，最明顯的成效之一是節省了大量電費。一些長期的機器運轉噪音也消失，改善了工作環境。」'
        },
    ]);

    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        arrows: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };

    const newsCardChange = (visible) => {
        if (visible && props.isCurrent) {
            setNewsShow(true)
        }
    }

    const shopCardChange = (visible) => {
        if (visible && props.isCurrent) {
            setShopShow(true)
        }
    }

    const tabChange = (index) => {
        setTabIndex(index)
    }

    const tagChange = (visible) => {
        if (visible && props.isCurrent) {
            setTagShow(true)
        }
    }

    const sitemapChange = (visible) => {
        if (visible && props.isCurrent) {
            setSitemapShow(true)
        }
    }

    const CardChange = (visible) => {
        if (visible && props.isCurrent) {
            setCardShow(true)
        }
    }

    const skillCardChange = (visible) => {
        if (visible && props.isCurrent) {
            setLifeShow(true)
        }
    }

    const promotionChange = (visible) => {
        if (visible && props.isCurrent) {
            setPromoShow(true)
        }
    }

    const marqueeChange = (visible) => {
        if (visible && props.isCurrent) {
            setMarqueeShow(true)
        }
    }

    const gotoEBUPlan = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/ebu-plan`)
        }, 300)
    }

    const scrollToPosition = () => {
        if (window.scrollY) {
            window.scroll(0, document.querySelector('#sitemap-2').offsetTop - 100);
            // window.scrollTop = (document.querySelector('#sitemap-1').offsetTop - 100)
        }
    }

    const gotoSurvey = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/survey/2`)
        }, 300)
    }

    return (
        <div>
            <section className="banner ebu">
                <div className="bg">
                    <img src={ebuBg} />
                </div>
                <div className="caption">
                    <div className="content">
                        <h1>別再追隨別人<br/>的腳步，將資金用在對的科技上 </h1>
                        <div className="action">
                            <button className="button bg-red">發現敏捷辦公室</button>
                            <button onClick={gotoSurvey} className="button outlined">免費諮詢</button>
                        </div>
                    </div>
                </div>
            </section>
            <section className="ebu-you-may-like">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrRight} height="246"/>
                </Parallax>
                <VisibilitySensor onChange={CardChange}> 
                    <div className="container">
                        <h3>您可能會有興趣</h3>
                        <div className={`scroller is-animate ${cardShow ? 'is-in':''}`}>
                            <img src={youLikeCards} height="417"/>
                        </div>
                        <div className="text-center" 
                        style={{marginTop: '-30px', position: 'relative', zIndex: 2}} 
                        onClick={scrollToPosition}>
                            <img src={EBUMore1} height='16' />
                        </div>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="ebu-you-life">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrLeft} height="246"/>
                </Parallax>
                <div className={`container`}>
                    <div className="header">
                        <div className="title">
                            <h3>趨勢觀點</h3>
                        </div>
                        <div className='action'>
                            看更多 
                            <img src={arrow} height="14" />
                        </div>
                    </div>
                    <VisibilitySensor onChange={skillCardChange} offset={'350px'} > 
                        <div style={{margin: '0 -15px'}} className={` is-animate ${cardShow ? 'is-in':''}`}>
                            <div className="tab-container">
                                <ul className="tab">
                                    {
                                        tabs.map((tab, index) => (
                                            <li className={tabIndex === index ? 'active' : ''} style={{background: 'none'}}>
                                                <a onClick={e => tabChange(index)}>
                                                    {tab.text}
                                                </a>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                            <img src={OpenSkill} width="100%"/>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section>
                <img src={EbuPromotion} width="100%" />
            </section>

            <section className="demo-shop">
                <div className="container">
                    <div className="header">
                        <div className="title">
                            <h3>選擇我們的頭家</h3> 
                        </div>
                        {/* <div className=></div> */}
                    </div>
                </div>

                <VisibilitySensor onChange={shopCardChange} partialVisibility={true}> 
                    <div className={`is-animate ${shopShow ? 'is-in':''}`}>
                        <Slider {...settings} className="demo-shop-carousel is-main">
                            {
                                demoShops.map((item, idx) => (
                                    <div key={`carousel-${idx}`} align="center" className='demo-shop-carousel-item'>
                                        <div style={{
                                            backgroundImage: `url(${item.background})`
                                        }}>
                                            <img src={item.logo} alt={item.name} height='80' />
                                            <img src={quote} className="quote" height='19' />
                                            <Typography variant="body1" component="p">
                                                {item.content}
                                            </Typography>
                                            <Typography variant="body1" component="p" className="shop-name">
                                                {item.meta!== '' ? `${item.meta}` : ''}<br/>
                                                <small>{item.name}</small>
                                            </Typography>
                                            
                                        </div>
                                    </div>
                                ))
                            }
                        </Slider>
                    </div>
                </VisibilitySensor>
                <VisibilitySensor onChange={marqueeChange} partialVisibility={true}> 
                    <div className={` is-animate ${marqueeShow ? 'is-in':''}`}>
                        <Marquee direction={'landscape'} loopData={[
                            {img: shopLogo1, height: 70}, {img: shopLogo2, height: 70}, {img: shopLogo3, height: 70}, {img: shopLogo4, height: 70}, {img: shopLogo5, height: 70}, {img: shopLogo6, height: 70}
                        ]}
                        />
                        <img src={shopPromo} width="100%" />
                    </div>
                </VisibilitySensor>
            </section>
            <section className='ebu-news'>
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrLeft} height="246"/>
                </Parallax>
                <Parallax y={[40, 0]} className="bg-img trangle">
                    <img src={newsTrangle} width="100%"/>
                </Parallax>
                <VisibilitySensor onChange={newsCardChange} partialVisibility={true}> 
                    <div className="container">
                       <h3>消息與公告</h3>
                        <div className={`is-animate ${newsShow ? 'is-in':''}`} style={{margin: '0 -15px'}}>
                            <img src={newsCards} height="288"/>
                        </div>
                        
                        <img src={ebuService} height="152"/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="img-sec" id="sitemap-2">
                <VisibilitySensor onChange={sitemapChange} partialVisibility={true}> 
                    <div className={` is-animate ${sitemapShow ? 'is-in':''}`}>
                        <img src={sitemap} width="100%"/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="img-sec">
                <VisibilitySensor onChange={tagChange} partialVisibility={true}> 
                    <div className={` is-animate ${tagShow ? 'is-in':''}`}>
                        <img src={ebuTags} width="100%"/>
                    </div>
                </VisibilitySensor>
            </section>
        </div>
    )
}


export default EBUBanner1;