import React from 'react';

import lifeBg from '../resources/images/banner-bg-4.png';
import chevronDown from '../resources/images/chevron-down.png';
import ArrowLeftImg from '../resources/images/arrow-left.png';
import ArrowRightImg from '../resources/images/arrow-right.png';
import promotionBg from "../resources/images/promotion-4.png";
import selectorMenu from "../resources/images/selector-menu-4.png";

const CBUBanner4 = (props) => {
    const [menuShow, setMenuShow] = React.useState(0);

    const menuOpen = (visible) => {
        setMenuShow(!menuShow)
    }

    const menuClose = () => {
        setMenuShow(false)
    }

    return (
        <div>
            <section className="banner">
                <div className="bg">
                    <img src={lifeBg} alt='lifbg'/>
                    <div className="anima-arrow left"><img src={ArrowLeftImg} height="133" alt='lif-arrow' /></div>
                    <div className="anima-arrow right"><img src={ArrowRightImg} height="84" alt='lif-arrow' /></div>
                </div>
                <div className="caption">
                    <div className="content">
                        <div className={`selector ${menuShow ? 'is-open' : ''}`}>
                            <button onClick={menuOpen} className="text" onBlur={menuClose}>
                            Uber Eats<br/>新客優惠金<img src={chevronDown} height="17" />
                            </button>
                            <div className="menu" onClick={menuClose}>
                                <img src={selectorMenu} width="375" />
                            </div>
                        </div>
                        <div className="action">
                            <button className="button bg-red"> 查看</button>
                        </div>
                        <div className="label">遠傳客戶</div>
                        <div className="label">白金會員</div>
                        <div className="label">friDay 專區</div>
                    </div>
                </div>
            </section>
            <section className={`promotion`} style={{transitionDelay: !props.isScrolling ? '2s': '0s'}} data-index={props.index}>
                <img src={promotionBg} />
            </section>
        </div>
    )
}

export default CBUBanner4;