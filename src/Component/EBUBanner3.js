import React from 'react';
import ebuBg from '../resources/images/ebu-bg-1.png';
import VisibilitySensor from 'react-visibility-sensor';
import { Parallax } from 'react-scroll-parallax';
import { Box, Container, Typography, Grid, Link } from '@material-ui/core';

import ebuArrLeft from '../resources/images/ebu-arrow-left.png';
import ebuArrRight from '../resources/images/ebu-arrow-right.png';

import OpenSkill from "../resources/images/open-skill.png";
import EbuPromotion from "../resources/images/ebu-promotion.png";

import youLikeCards from "../resources/images/ebu-you-like-cards.png";
import arrow from '../resources/images/chevron-right-thin.png';

const EBUBanner1 = (props) => {
    const [cardShow, setCardShow] = React.useState(false);
    const [lifeShow, setLifeShow] = React.useState(false);

    const CardChange = (visible) => {
        if (visible && !window.loading) {
            setCardShow(true)
        }
    }

    const skillCardChange = (visible) => {
        if (visible && !window.loading) {
            setLifeShow(true)
        }
    }

    return (
        <div>
            <section className="banner ebu">
                <div className="bg">
                    <img src={ebuBg} />
                </div>
                <div className="caption">
                    <div className="content">
                        <h1>頭家輕鬆配<br/>幫您策劃開店的大小事！</h1>
                        <div className="action">
                            <button className="button bg-red">策劃去</button>
                            <button className="button outlined">免費諮詢</button>
                        </div>
                    </div>
                </div>
            </section>
            <section className="ebu-you-may-like">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrRight} height="246"/>
                </Parallax>
                <VisibilitySensor onChange={CardChange}> 
                    <div className="container">
                        <h3>你可能會有興趣</h3>
                        <div className={`scroller is-animate ${cardShow ? 'is-in':''}`}>
                            <img src={youLikeCards} height="417"/>
                        </div>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="ebu-you-life">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrLeft} height="246"/>
                </Parallax>
                <div className={`container`}>
                    <div className="header">
                        <div className="title">
                            <h3>開業技法新知</h3>
                        </div>
                        <div className='action'>
                            看更多 
                            <img src={arrow} height="14" />
                        </div>
                    </div>
                    <VisibilitySensor onChange={skillCardChange}> 
                        <div style={{margin: '0 -15px'}} className={` is-animate ${cardShow ? 'is-in':''}`}>
                            <div className="tab-container">
                                <ul className="tab">
                                    <li className="active">
                                        <a>餐飲業</a>
                                    </li>
                                    <li>
                                        <a>零售業</a>
                                    </li>
                                    <li>
                                        <a>美容業</a>
                                    </li>
                                    <li>
                                        <a>全部</a>
                                    </li>
                                </ul>
                            </div>
                            <img src={OpenSkill} width="100%"/>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section>
                <img src={EbuPromotion} width="100%" />
            </section>
        </div>
    )
}


export default EBUBanner1;