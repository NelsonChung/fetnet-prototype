import React from 'react';
import {useHistory, useLocation} from 'react-router-dom';

import CBUBanner3Bg from '../resources/images/CBUBanner-3-bg.png';
import chevronDown from '../resources/images/chevron-down.png';
import ArrowLeftImg from '../resources/images/arrow-left.png';
import ArrowRightImg from '../resources/images/arrow-right.png';
import promotionBg from "../resources/images/promotion-3.png";
import selectorMenu from "../resources/images/selector-menu-3.png";

const CBUBanner3 = (props) => {
    const [menuShow, setMenuShow] = React.useState(0);
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';

    const menuOpen = (visible) => {
        setMenuShow(!menuShow)
    }

    const menuClose = () => {
        setMenuShow(false)
    }

    const goYouLife = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/yo-life`)
        }, 300)
    }

    return (
        <div>
            <section className="banner">
                <div className="bg">
                    <img src={CBUBanner3Bg}/>
                    <div className="anima-arrow left"><img src={ArrowLeftImg} height="133" /></div>
                    <div className="anima-arrow right"><img src={ArrowRightImg} height="84" /></div>
                </div>
                <div className="caption">
                    <div className="content">
                        <div className={`selector ${menuShow ? 'is-open' : ''}`}>
                            <button onClick={menuOpen} className="text" onBlur={menuClose}>
                                Yo! 活，<br/>數位生活精彩加值
                                <img src={chevronDown} height="17" />
                            </button>
                            <div className="menu" onClick={menuClose}>
                                <img src={selectorMenu} width="375" />
                            </div>
                        </div>
                        <div className="action">
                            <button onClick={goYouLife} className="button bg-red"> 查看</button>
                        </div>
                        <div className="label">逛逛看</div>
                        <div className="label">小資旅遊GO!</div>
                        <div className="label">週末夜</div>
                    </div>
                </div>
            </section>
            <section className={`promotion`} style={{transitionDelay: !props.isScrolling ? '2s': '0s'}} data-index={props.index}>
                <img src={promotionBg} />
            </section>
        </div>
    )
}

export default CBUBanner3;