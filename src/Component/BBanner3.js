import React from 'react';
import Slider from "react-slick";

import bannerImg1 from '../resources/images/yo-life-back.png'
import bannerImg2 from '../resources/images/yo-life-front.png'

const BBanner3 = (props) => {
    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };
    return (
        <section className="banner">
            <Slider autoplay={false} {...settings} dots={true} arrows={false}>
                <div>
                    <div className="image">
                        <img src={bannerImg1} alt="banner-1" height='354' className='back' />
                        <img src={bannerImg2} alt="banner-2" height='354' className='front' />
                    </div>
                    <div className='caption'>
                        <div className="content">
                            <h1>Yo! 活，與你共度日常</h1>
                            <p>一起床就是要追劇，今天要看哪一部呢？</p>
                        </div>
                        <div className="text-center">
                            <button className="button bg-red rounded">看更多</button>
                        </div>
                    </div>
                </div>
            </Slider>
            <ul className="slick-dots" style={{display: 'block'}}><li class="slick-active"><button>1</button></li><li class=""><button>2</button></li><li class=""><button>3</button></li></ul>
        </section>
    );
}

export default BBanner3;