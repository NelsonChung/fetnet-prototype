import React from 'react';

import bannerImg1 from '../resources/images/A-ebu-banner-back.png'
import bannerImg2 from '../resources/images/A-ebu-banner-front.png'
import arrow from '../resources/images/chevron-right-thin.png'

const AEBUBanner1 = (props) => {

    return (
        <section className="banner">
            <div className="image">
                <img src={bannerImg1} alt="banner-1" height='522' className='back' />
                <img src={bannerImg2} alt="banner-2" height='522' className='front' />
            </div>
            <div className='caption'>
                <div className="content narrow">
                    <h1>頭家輕鬆配<br/>幫您策劃開店的大小事！</h1>
                </div>
                <div className="text-center">
                    <button className="button bg-red rounded">看更多</button>
                    <a className='more'>
                        立即評估創業成功率
                        <img src={arrow} height="14" />
                    </a>
                </div>
            </div>
        </section>
    );
}

export default AEBUBanner1;