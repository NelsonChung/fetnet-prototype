import React from 'react';

import lifeBg from '../resources/images/life-bg.png';
import chevronDown from '../resources/images/chevron-down.png';
import ArrowLeftImg from '../resources/images/arrow-left.png';
import ArrowRightImg from '../resources/images/arrow-right.png';
import promotionBg from "../resources/images/promotion-1.png";
import serviceFox from "../resources/images/service-fox.png";
import fetnetArr from "../resources/images/fetnet-arrow.png";
import selectorMenu from "../resources/images/selector-menu-1.png";

const CBUBanner1 = (props) => {
    const [menuShow, setMenuShow] = React.useState(0);
    
    const menuOpen = (visible) => {
        setMenuShow(!menuShow)
    }

    const menuClose = () => {
        setMenuShow(false)
    }

    return (
        <div>
            <section className="banner">
                <div className="bg">
                    <img src={lifeBg}/>
                    <div className="anima-arrow left"><img src={ArrowLeftImg} height="133" /></div>
                    <div className="anima-arrow right"><img src={ArrowRightImg} height="84" /></div>
                </div>
                <div className="caption">
                    <div className="content">
                        <div className={`selector ${menuShow ? 'is-open' : ''}`}>
                            <button onClick={menuOpen} className="text" onBlur={menuClose}>
                                地表最佳拍照<br/>手機大比拼<img src={chevronDown} height="17" />
                            </button>
                            <div className="menu" onClick={menuClose}>
                                <img src={selectorMenu} width="375" />
                            </div>
                        </div>
                        <div className="action">
                            <button className="button bg-red"> 查看</button>
                        </div>
                        <div className="label">網路門市</div>
                        <div className="label">iPhone 11</div>
                        <div className="label">Note 10</div>
                    </div>
                </div>
            </section>
            <section className={`promotion`} style={{transitionDelay: !props.isScrolling ? '2s': '0s'}} data-index={props.index}>
                <img src={promotionBg}/>
            </section>
        </div>
    )
}

export default CBUBanner1;