import React from 'react';
import Slider from "react-slick";

import bannerImg1 from '../resources/images/banner-4-back.png'
import bannerImg2 from '../resources/images/banner-4-front.png'

const BBanner4 = (props) => {
    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };
    return (
        <section className="banner">
            <Slider autoplay={false} {...settings} dots={true} arrows={false}>
                <div>
                    <div className="image">
                        <img src={bannerImg1} alt="banner-1" height='375' className='back' />
                        <img src={bannerImg2} alt="banner-2" height='375' className='front' />
                    </div>
                    <div className='caption'>
                        <div className="content">
                            <h1>上網語音 199 夠用最實在</h1>
                            <p>月付199，單門號或搭家電任你選</p>
                        </div>
                        <div className="text-center">
                            <button className="button bg-red rounded">看更多</button>
                        </div>
                    </div>
                </div>
            </Slider>
            <ul className="slick-dots" style={{display: 'block'}}><li class="slick-active"><button>1</button></li><li class=""><button>2</button></li><li class=""><button>3</button></li></ul>
        </section>
    );
}

export default BBanner4;