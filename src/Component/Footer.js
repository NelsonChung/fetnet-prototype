import React from 'react';
import {useLocation, useHistory} from 'react-router-dom';
import FooterAImg from '../resources/images/footerA.png';
import FooterBImg from '../resources/images/footerB.png';
import FooterAEBU from '../resources/images/ebu-footer.png';
import serviceGirl1 from '../resources/images/helper-girl-box-1.png';
import serviceGirl2 from '../resources/images/helper-girl-box-2.png';
import surveyFooter from '../resources/images/survey-footer.png';

const Footer = (props) => {
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';
    const isEBU = location.pathname.indexOf('ebu') > -1;
    const isSurvey = location.pathname.indexOf('survey') > -1;
    return (
        <footer>
            {
                concept==="A" ? <img src={isEBU ? serviceGirl2 : serviceGirl1} className="service-button" /> : ''
            }
            <img src={concept === 'B' ? FooterAImg : (isEBU ? FooterAEBU : (isSurvey ? surveyFooter : FooterBImg))} width="100%" alt="footer" />
        </footer>
    )
}

export default Footer;