import React from 'react';
import Slider from "react-slick";

import bannerImg1 from '../resources/images/banner-img-1.png'
import bannerImg2 from '../resources/images/banner-img-2.png'

const BBanner1 = (props) => {

    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };

    return (
        <section className="banner">
            <Slider autoplay={false} {...settings} dots={true} arrows={false}>
                <div>
                    <div className="image">
                        <img src={bannerImg1} alt="banner-1" height='330' className='back' />
                        <img src={bannerImg2} alt="banner-2" height='390' className='front' />
                    </div>
                    <div className='caption'>
                        <div className="content">
                            <h1>遠傳 iPhone11 享樂1+1</h1>
                            <p>帶走最新AirPods，免費再送 friDay影音</p>
                        </div>
                        <div className="text-center">
                            <button className="button bg-red rounded">看更多</button>
                        </div>
                    </div>
                </div>
            </Slider>
            <ul className="slick-dots" style={{display: 'block'}}><li class="slick-active"><button>1</button></li><li class=""><button>2</button></li><li class=""><button>3</button></li></ul>
        </section>
    );
}

export default BBanner1;