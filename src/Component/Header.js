import React from 'react';
import {useHistory, useLocation} from 'react-router-dom';

import hambuger from '../resources/images/hambger.png';
import hambugerA from '../resources/images/hambugerA.png';
import logo from '../resources/images/fetnet-logo.png';
import searchIcon from '../resources/images/search.png';
import searchIconA from '../resources/images/searchA.png';
import closeIcon from '../resources/images/icon-close.png';
import closeIconA from '../resources/images/closeIconA.png';
import searchClose from '../resources/images/search-close.png';
import heartIcon from '../resources/images/icon-heart.png';
import sideMenuB1 from '../resources/images/sidebar1.png';
import sideMenuB2 from '../resources/images/sidebar2.png';
import sideMenuA from '../resources/images/sidebarA.png';
import searchField from '../resources/images/saerch-field.png';
import searchLink from '../resources/images/link.png';

import searchIcon1 from '../resources/images/search-icon-1.png';
import searchIcon2 from '../resources/images/search-icon-2.png';
import searchIcon3 from '../resources/images/search-icon-3.png';
import searchTags from '../resources/images/search-tags.png';

import menuCards from '../resources/images/menu-cards.png';
import menuContent from '../resources/images/menu-content.png';
import down from '../resources/images/menu-chevron-down.png';
import EBUMenuFooter from '../resources/images/EBU-menu-footer.png';
import EBUMenuHeader from '../resources/images/EBU-menu-header.png';
import EBUMenuList from '../resources/images/ebu-menu-list.png';

import BMenuTop from '../resources/images/B-menu-top.png';
import BMenuFooter from '../resources/images/B-menu-footer.png';
import BPlus from '../resources/images/B-plus.png';
import BMinus from '../resources/images/B-minus.png';
import BMenuRight from '../resources/images/B-menu-right.png';
import BMenuContent1 from '../resources/images/B-menu-content-1.png';
import BMenuContent2 from '../resources/images/B-menu-content-2.png';

const Header = (props) => {
    const [sidebarShow, setSidebarShow] = React.useState(false);
    const [searchShow, setSearchShow] = React.useState(false);
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';
    const isEBU = location.pathname.indexOf('ebu') > -1;

    const [checker, setChecker] = React.useState(concept==='A' && location.pathname.indexOf('ebu') > -1);
    const [isMenuOpen, setMenuOpen] = React.useState(false);
    const [bMenuOpen1, setBMenuOpen1] = React.useState(false);
    const [bMenuOpen2, setBMenuOpen2] = React.useState(false);

    history.listen(listener => {
        setChecker(concept==='A' && location.pathname.indexOf('ebu') > -1)
    })

    const toggleMenuItem1 = (index) => {
        setBMenuOpen1(!bMenuOpen1)
    }

    const toggleMenuItem2 = (index) => {
        setBMenuOpen2(!bMenuOpen2)
    }

    const toggleMenu = () => {
        setMenuOpen(!isMenuOpen)
    }

    const closeSearch = () => {
        setSearchShow(false)
    }
    const openSearch = () => {
        document.getElementsByTagName('html')[0].classList.add('sidebar-open');
        setSearchShow(true)
    }

    const showSidebar = () => {
        document.getElementsByTagName('html')[0].classList.add('sidebar-open');
        setSidebarShow(true)
    }

    const closeSidebar = () => {
        setSidebarShow(false)
        setTimeout(() => {
            document.getElementsByTagName('html')[0].classList.remove('sidebar-open');
        }, 300)
    }

    const gotoHelper = (path) => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/helper-center`)
            menuClose()
        }, 300)
    }

    const gotoIndex = (path) => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/index`)
        }, 300)
    }

    const changeLink = (path) => {
        history.push(`/concept${concept}/${path}`)
        setTimeout(() => {
            menuClose()
        }, 500)
    }
    
    const redirectEbu = (e) => {
        const isChecked = e.target.checked;
        setChecker(isChecked)
        setTimeout(() => {
            if(isChecked) {
                history.push(`/concept${concept}/ebu`)
            }
            else {
                history.push(`/concept${concept}/index`)
            }
            setTimeout(() => {
                menuClose()
            }, 500)
        }, 500)
    }

    const menuClose = () => {
        closeSearch()
        closeSidebar()
        document.getElementsByTagName('html')[0].classList.add('sidebar-open');
    }

    const menuList = () => {
        if ( concept==='A' ) {
            if ( !isEBU ) {
                return (
                    <div className="menu">
                        <div className={`item ${isMenuOpen ? 'menu-open' : ''}`}>
                            <div className='menu-header' onClick={toggleMenu}>
                                <div className="content">產品服務</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content">
                                <img src={menuContent} width="375" />
                                <div className='scroll'>
                                    <img src={menuCards} height="146" />
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">資費方案</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">YO 活!</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">優惠</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">幫助中心</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">我的專區</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className={`menu ${isEBU ? 'is-blue' : ''}`}>
                        <div className={`item`}>
                            <div className='menu-header'>
                                <div className="content">微型店家解決方案</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content">
                            </div>
                        </div>
                        <div className={`item ${isMenuOpen ? 'menu-open' : ''}`}>
                            <div className='menu-header' onClick={toggleMenu}>
                                <div className="content">中型企業解決方案</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content">
                                <img src={EBUMenuList} width="375" />
                            </div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">大型企業解決方案</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">公部門解決方案</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">產品與服務</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">趨勢觀點</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">幫助中心</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                        <div className="item">
                            <div className='menu-header'>
                                <div className="content">5G</div>
                                <div className="action">
                                    <img src={down} height="7"/>
                                </div>
                            </div>
                            <div className="menu-content"></div>
                        </div>
                    </div>
                )
            }
        } else {
            return (
                <div className='menu'>
                    <div className={`item is-collapse ${!bMenuOpen1 ? '' : 'menu-open'}`}>
                        <div className="menu-header" onClick={e => toggleMenuItem1()}>
                            <div className='content'>主題企劃</div>
                            <div className='action'>
                                <img src={BPlus} className='plus' width="15" />
                                <img src={BMinus} className='minus' width="15" />
                            </div>
                        </div>
                        <div className="menu-content">
                            <img src={BMenuContent1} width="100%" />
                        </div>
                    </div>
                    <div className={`item is-collapse ${!bMenuOpen2 ? '' : 'menu-open'}`}>
                        <div className="menu-header" onClick={e => toggleMenuItem2()}>
                            <div className='content'>產品資費</div>
                            <div className='action'>
                                <img src={BPlus} className='plus' width="15" />
                                <img src={BMinus} className='minus' width="15" />
                            </div>
                        </div>
                        <div className="menu-content">
                            <img src={BMenuContent2} width="100%" />
                        </div>
                    </div>
                    <div className="item">
                        <div className="menu-header">
                            <div className='content'>優惠</div>
                            <div className='action'>
                                <img src={BMenuRight} height="14" />
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="menu-header">
                            <div className='content'>幫助中心</div>
                            <div className='action'>
                                <img src={BMenuRight} height="14" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }

    return (
        <header className={`concept${concept}`}>
            {
                (concept==='B')
                ? (
                    <div className="nav-bar">
                        <div className="nav-left">
                            <img src={logo} alt="fetnetLogo" height="30"/>
                        </div>
                        <div className="nav-right">
                            <a><img src={searchIconA} alt="search" height="24"/></a>
                            <a onClick={showSidebar}><img src={hambugerA} alt="hambuger" height="24"/></a>
                        </div>
                    </div>
                )
                : (
                <div className="nav-bar">
                    <div className="nav-left">
                        <a onClick={showSidebar}><img src={hambuger} alt="hambuger" height="24" alt='' /></a>
                    </div>
                    <div className="nav-center"><img src={logo} alt="fetnetLogo" onClick={gotoIndex} height="30"/></div>
                    <div className="nav-right">
                        <a onClick={openSearch}><img src={searchIcon} alt="search" height="24"/></a>
                    </div>
                </div>
                )
            }
            <div className={`sidebar ${sidebarShow ? 'is-open': ''}`}>
                {
                    (concept === 'B') 
                    ? (
                        <div className="nav-menu">
                            <div onClick={e => changeLink('index')} className={`item ${location.pathname.indexOf('ebu') > -1 ? '' : 'active'}`}>個人</div>
                            <div onClick={e => changeLink('ebu')} className={`item ${location.pathname.indexOf('ebu') > -1 ? 'active' : ''}`}>商務</div>
                            <div className={`item ${location.pathname.indexOf('yolife') > -1 ? 'active' : ''}`}>Yo! 活</div>
                            <div className="close" onClick={closeSidebar}>
                                <img src={closeIconA} height="24" alt='close' />
                            </div>
                        </div>
                    ) 
                    : (
                        <div className="nav-bar">
                            <div className="nav-left">
                                <a onClick={closeSidebar}>
                                    <img src={closeIcon} height="28" />
                                </a>
                            </div>
                            <div className="nav-center">
                                <label className="switch" htmlFor='is-ebu'>
                                    <input type='checkbox' name="is_ebu" value='1' id='is-ebu' onChange={redirectEbu} checked={checker}  />
                                    <div className="content">
                                        <span className="off">個人</span>
                                        <span className="on">商務</span>
                                    </div>
                                </label>
                            </div>
                            <div className="nav-right">
                                <img src={heartIcon} height="28" />
                            </div>
                        </div>
                    )
                }
                {
                    concept==="A" ? <div onClick={gotoHelper} className="helper-link"></div> : ''
                }
                
                <img src={concept==='B' ? BMenuTop : (!isEBU ? sideMenuB1 : EBUMenuHeader)} width="100%" />
                {
                    menuList()
                }
                {
                    concept==='A' 
                    ? (!isEBU ? <img src={sideMenuB2} width="100%" /> : <img src={EBUMenuFooter} width="100%" />)
                    : <img src={BMenuFooter} width="100%" />
                }
            </div>
            <div className={`search-panel ${searchShow ? 'is-open' : ''}`}>
                <div className="search-header">
                    <a onClick={menuClose}>
                        <img src={searchClose} height="17" />
                    </a>
                </div>
                <div className="search-body">
                    <div className="container">
                        <h1>想要找什麼？</h1>
                    </div>
                    <img src={searchField} width="375"/>
                    <div className="container">
                        <div className="search-icons">
                            <img src={searchIcon1} width="64px" height="93" />
                            <div onClick={gotoHelper}><img src={searchIcon2} width="64px" height="93" /></div>
                            <img src={searchIcon3} width="64px" height="93" />
                        </div>
                        <img src={searchTags} width="332" />
                    </div>
                </div>
                <div className="search-footer text-center">
                    <img src={searchLink} width="160"/>
                </div>
            </div>
        </header>
    )
}

export default Header;