import React from 'react'
import { useHistory, useLocation } from 'react-router-dom';

const Loader = (props) => {
    const history = useHistory();
    const location = useLocation();
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';
    const html = document.getElementsByTagName('html')[0];

    if (!html.classList.contains(`concept${concept}`)) {
        html.classList.add(`concept${concept}`)
    } 

    history.listen(listener => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            document.getElementsByTagName('html')[0].classList.remove('fade-out');
            document.body.classList.add('is-loading');
            loadingAnimate();
        }, 500)
    })

    const loadingAnimate = () => {
        window.loading = true;
        setTimeout(() => {
            document.getElementsByTagName('html')[0].classList.remove('is-loading');
            document.body.classList.add('is-loading-show');
            document.body.classList.remove('is-loading');
            setTimeout(() => {
                document.body.classList.remove('is-loading-show');
            }, 500)
            setTimeout(() => {
                window.loading = false;
            }, 1000)
        }, 500);
    }
    
    window.onload = () => {
        loadingAnimate()
    }

    return (
        <section className="loading"></section>
    );
}

export default Loader;