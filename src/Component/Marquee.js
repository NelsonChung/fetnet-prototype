import React, { Component } from 'react';
import PropTypes from 'prop-types';

const raf = () => {
  var lastTime = 0;
  var vendors = ['webkit', 'moz'];
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame =
      window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function (callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function () { callback(currTime + timeToCall); },
        timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };

  if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function (id) { clearTimeout(id) };
}

raf();

export default class Marquee extends Component {
  static defaultProps = {
    direction: 'landscape',
    verticalItemHeight: '60px'
  }

  constructor(props) {
    super(props);

    this.timerMarquee = null;
    this.domMi = null;
    this.domMw = null;

    this.state = {};
  }

  initMarquee = () => {
    this.stopMarquee();
    this.runMarquee();
  }

  //横向滚动
  landscapeMarquee = () => {
    this.domMw.scrollLeft >= this.domMi.scrollWidth ? this.domMw.scrollLeft = 0 : this.domMw.scrollLeft++;
    this.timerMarquee = requestAnimationFrame(this.landscapeMarquee);
  }

  //竖向滚动
  verticalMarquee = () => {
    this.domMw.scrollTop >= this.domMi.scrollHeight ? this.domMw.scrollTop = 0 : this.domMw.scrollTop++;
    this.timerMarquee = requestAnimationFrame(this.verticalMarquee);
  }

  // 运动
  runMarquee = () => {
    this.stopMarquee();
    if (this.props.direction === 'vertical') {
      this.timerMarquee = requestAnimationFrame(this.verticalMarquee);
    } else {
      this.timerMarquee = requestAnimationFrame(this.landscapeMarquee);
    }
  }

  //暂停
  stopMarquee = () => {
    this.timerMarquee && cancelAnimationFrame(this.timerMarquee)
  }

  componentDidMount = () => {
    this.initMarquee();

    let { getMarquee } = this.props

    getMarquee && getMarquee({
      runMarquee: this.runMarquee,
      stopMarquee: this.stopMarquee
    });

  }

  componentWillUnmount = () => {
    this.stopMarquee();
  }

  renderLandscapeMarquee() {
    let { loopData } = this.props;

    return (
      <div className="marquee-landscape-wrap" ref={(mw) => { this.domMw = mw; }}>
        <div className="marquee-landscape-item" ref={(mi) => { this.domMi = mi; }}>
          {loopData.map((item, index) => (
            <div className="marquee-landscape-txt" key={index}>
              <img src={item.img} height={item.height}/>
              {
                item.text 
                ? (<div className='text'>{item.text}</div>)
                : ''
              }
            </div>
          ))}
        </div>
        <div className="marquee-landscape-item">
          {loopData.map((item, index) => (
            <div className="marquee-landscape-txt" key={index}>
              <img src={item.img} height={item.height}/>
              {
                item.text 
                ? (<div></div>)
                : ''
              }
            </div>
          ))}
        </div>
      </div>
    )
  }

  renderVerticalMarquee() {
    let { loopData, verticalItemHeight } = this.props;
    return (
      <div className="marquee-vertical-wrap" ref={(mw) => { this.domMw = mw; }}>
        <div className="marquee-vertical-item" ref={(mi) => { this.domMi = mi; }}>
          {loopData.map((item, index) => (<div style={{ height: verticalItemHeight, lineHeight: verticalItemHeight }} className="marquee-vertical-txt" key={index}>{item.txt}</div>))}
        </div>
        <div className="marquee-vertical-item">
          {loopData.map((item, index) => (<div style={{ height: verticalItemHeight, lineHeight: verticalItemHeight }} className="marquee-vertical-txt" key={index}>{item.txt}</div>))}
        </div>
      </div>
    )
  }

  render() {

    let { direction } = this.props;

    return (
      <div className="react-marquee-box">
        {direction === 'landscape' ? this.renderLandscapeMarquee() : this.renderVerticalMarquee()}
      </div>
    )
  }
}

Marquee.propTypes = {
  loop: PropTypes.bool,
  loopData: PropTypes.array,
  getMarquee: PropTypes.func,
  direction: PropTypes.string,
  verticalItemHeight: PropTypes.string
};