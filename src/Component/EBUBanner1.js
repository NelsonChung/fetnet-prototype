import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import ebuBg from '../resources/images/ebu-banner-1.png';
import VisibilitySensor from 'react-visibility-sensor';
import { Parallax } from 'react-scroll-parallax';
import { Typography } from '@material-ui/core';
import Slider from "react-slick";
import Marquee from "../Component/Marquee";

import ebuArrLeft from '../resources/images/ebu-arrow-left.png';
import ebuArrRight from '../resources/images/ebu-arrow-right.png';
import ebuService from "../resources/images/ebu-service.png";
import newsTrangle from "../resources/images/news-bottom-trangle.png";
import newsCards from "../resources/images/ebu-news-card.png";
import ebuTags from "../resources/images/ebu-tags.png";
import demoShopBg from "../resources/images/demo-shop-bg.png";
import shopLogo1 from "../resources/images/shop-1-logo.png";
import shopLogo2 from "../resources/images/shop-2-logo.png";
import shopLogo3 from "../resources/images/shop-3-logo.png";
import shopLogo4 from "../resources/images/shop-4-logo.png";
import shopLogo1White from "../resources/images/shop-1-logo-white.png";
import shopLogo2White from "../resources/images/shop-2-logo-white.png";
import shopLogo3White from "../resources/images/shop-3-logo-white.png";
import shopLogo4White from "../resources/images/shop-4-logo-white.png";
import quote from "../resources/images/A-ebu-quote.png";

import shopPromo from "../resources/images/shop-promotion.png";

import OpenSkill from "../resources/images/open-skill-1.png";
import EbuPromotion from "../resources/images/ebu-promotion.png";

import youLikeCards from "../resources/images/ebu-you-like-cards.png";
import EBUMore1 from "../resources/images/ebu-more-1.png";
import arrow from '../resources/images/chevron-right-thin.png';
import sitemap from '../resources/images/ebu-sitemap-1.png';

const EBUBanner1 = (props) => {
    const [cardShow, setCardShow] = React.useState(false);
    const [lifeShow, setLifeShow] = React.useState(false);
    const tabs = [
        {text: '全部'},
        {text: '餐飲業'},
        {text: '零售業'},
        {text: '美容業'}
    ];
    let history = useHistory();
    const location = useLocation()
    const concept = location.pathname.indexOf('conceptB') > -1 ? 'B' : 'A';

    const [tabIndex, setTabIndex] = React.useState(0);
    const [shopShow, setShopShow] = React.useState(false);
    const [newsShow, setNewsShow] = React.useState(false);
    const [promoShow, setPromoShow] = React.useState(false);
    const [marqueeShow, setMarqueeShow] = React.useState(false);
    const [sitemapShow, setSitemapShow] = React.useState(false);
    const [tagShow, setTagShow] = React.useState(false);

    const [demoShops] = React.useState([
        {
            background: demoShopBg,
            logo: shopLogo1White,
            name: '花草慢時光',
            meta: '',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo2White,
            name: 'TH Korea Salon',
            meta: 'Find The Life Kitchen',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」' 
        },
        {
            background: demoShopBg,
            logo: shopLogo3White,
            name: '楓露',
            meta: 'Find The Life Kitchen',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
        {
            background: demoShopBg,
            logo: shopLogo4White,
            name: 'The 41 Bistro',
            meta: '肆拾壹號英式小酒館',
            content: '「Smart Wifi 的功能很多樣且彈性，可以自由選擇、設定。對於沒有多餘行銷人力的小店家來說很方便，取代以往自己設計活動耗費的時間。」'
        },
    ]);

    const settings = {
        infinite: true,
        speed: 1000,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    };

    const newsCardChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setNewsShow(true)
        }
    }

    const shopCardChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setShopShow(true)
        }
    }

    const tabChange = (index) => {
        setTabIndex(index)
    }

    const tagChange = (visible) => {
        if (visible && props.isCurrent) {
            setTagShow(true)
        }
    }

    const sitemapChange = (visible) => {
        if (visible && props.isCurrent) {
            setSitemapShow(true)
        }
    }

    const CardChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setCardShow(true)
        }
    }

    const skillCardChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setLifeShow(true)
        }
    }

    const promotionChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setPromoShow(true)
        }
    }

    const marqueeChange = (visible) => {
        if (visible && props.isCurrent && !window.loading) {
            setMarqueeShow(true)
        }
    }

    const gotoEBUPlan = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/ebu-plan`)
        }, 300)
    }

    const gotoSurvey = () => {
        document.getElementsByTagName('html')[0].classList.add('fade-out');
        setTimeout(() => {
            history.push(`/concept${concept}/survey/1`)
        }, 300)
    }

    const scrollToPosition = () => {
        if (window.scrollY) {
            window.scroll(0, document.querySelector('#sitemap-1').offsetTop - 100);
            // window.scrollTop = (document.querySelector('#sitemap-1').offsetTop - 100)
        }
    }

    return (
        <div>
            <section className="banner ebu">
                <div className="bg">
                    <img src={ebuBg} />
                </div>
                <div className="caption">
                    <div className="content">
                        <h1>頭家輕鬆配<br/>幫您策劃開店的大小事！</h1>
                        <div className="action">
                            <button onClick={gotoEBUPlan} className="button bg-red">策劃去</button>
                            <button onClick={gotoSurvey} className="button outlined">免費諮詢</button>
                        </div>
                    </div>
                </div>
            </section>
            <section className="ebu-you-may-like">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrRight} height="246"/>
                </Parallax>
                <VisibilitySensor onChange={CardChange}> 
                    <div className="container">
                        <h3>您可能會有興趣</h3>
                        <div className={`scroller is-animate ${cardShow ? 'is-in':''}`}>
                            <img src={youLikeCards} height="417"/>
                        </div>
                        <div className="text-center" 
                        style={{marginTop: '-30px', position: 'relative', zIndex: 2}} 
                        onClick={scrollToPosition}>
                            <img src={EBUMore1} height='16' />
                        </div>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="ebu-you-life">
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrLeft} height="246"/>
                </Parallax>
                <div className={`container`}>
                    <div className="header">
                        <div className="title">
                            <h3>開業技法新知</h3>
                        </div>
                        <div className='action'>
                            看更多 
                            <img src={arrow} height="14" />
                        </div>
                    </div>
                    <VisibilitySensor onChange={skillCardChange} offset={'350px'} > 
                        <div style={{margin: '0 -15px'}} className={` is-animate ${cardShow ? 'is-in':''}`}>
                            <div className="tab-container">
                                <ul className="tab">
                                    {
                                        tabs.map((tab, index) => (
                                            <li className={tabIndex === index ? 'active' : ''} style={{background: 'none'}}>
                                                <a onClick={e => tabChange(index)}>
                                                    {tab.text}
                                                </a>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                            <img src={OpenSkill} width="100%"/>
                        </div>
                    </VisibilitySensor>
                </div>
            </section>
            <section>
                <VisibilitySensor onChange={promotionChange} partialVisibility={true}> 
                    <div className={` is-animate ${promoShow ? 'is-in':''}`}>
                        <img src={EbuPromotion} width="100%" />
                    </div>
                </VisibilitySensor>
            </section>

            <section className="demo-shop">
                <div className="container">
                    <div className="header">
                        <div className="title">
                            <h3>選擇我們的頭家</h3> 
                        </div>
                        {/* <div className=></div> */}
                    </div>
                </div>

                <VisibilitySensor onChange={shopCardChange} partialVisibility={true}> 
                    <div className={`is-animate ${shopShow ? 'is-in':''}`}>
                        <Slider autoplay={true} {...settings} arrows={true} className="demo-shop-carousel is-main" dots={true}>
                            {
                                demoShops.map((item, idx) => (
                                    <div key={`carousel-${idx}`} align="center" className='demo-shop-carousel-item'>
                                        <div style={{
                                            backgroundImage: `url(${item.background})`
                                        }}>
                                            <img src={item.logo} alt={item.name} height='80' />
                                            <img src={quote} className="quote" height='19' />
                                            <Typography variant="body1" component="p">
                                                {item.content}
                                            </Typography>
                                            <Typography variant="body1" component="p" className="shop-name">
                                                {item.name}  {item.meta!== '' ? `- ${item.meta}` : ''}
                                            </Typography>
                                        </div>
                                    </div>
                                ))
                            }
                        </Slider>
                    </div>
                </VisibilitySensor>
                <VisibilitySensor onChange={marqueeChange} partialVisibility={true}> 
                    <div className={` is-animate ${marqueeShow ? 'is-in':''}`}>
                        <Marquee direction={'landscape'} loopData={[
                            {img: shopLogo1, height: 112}, {img: shopLogo2, height: 112}, {img: shopLogo3, height: 112}, {img: shopLogo4, height: 112}
                        ]}
                        />
                        <img src={shopPromo} width="100%" />
                    </div>
                </VisibilitySensor>
            </section>
            <section className='ebu-news'>
                <Parallax y={[-20, 20]} className="bg-img">
                    <img src={ebuArrLeft} height="246"/>
                </Parallax>
                <Parallax y={[40, 0]} className="bg-img trangle">
                    <img src={newsTrangle} width="100%"/>
                </Parallax>
                <VisibilitySensor onChange={newsCardChange} partialVisibility={true}> 
                    <div className="container">
                        <div className="header">
                            <div className="title">
                                <h3>消息與公告</h3>
                            </div>
                            <div className='action'>
                                看更多 
                                <img src={arrow} height="14" />
                            </div>
                        </div>
                        <div className={`is-animate ${newsShow ? 'is-in':''}`} style={{margin: '0 -15px'}}>
                            <img src={newsCards} height="288"/>
                        </div>
                        <img src={ebuService} height="152"/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="img-sec" id='sitemap-1'>
                <VisibilitySensor onChange={sitemapChange} partialVisibility={true}> 
                    <div className={` is-animate ${sitemapShow ? 'is-in':''}`}>
                        <img src={sitemap} width="100%"/>
                    </div>
                </VisibilitySensor>
            </section>
            <section className="img-sec">
                <VisibilitySensor onChange={tagChange} partialVisibility={true}> 
                    <div className={` is-animate ${tagShow ? 'is-in':''}`}>
                        <img src={ebuTags} width="100%"/>
                    </div>
                </VisibilitySensor>
            </section>
        </div>
    )
}


export default EBUBanner1;