import React from 'react';

import CBUBanner5Bg from '../resources/images/cbu-tab5-banner.png';
import ArrowLeftImg from '../resources/images/arrow-left.png';
import ArrowRightImg from '../resources/images/arrow-right.png';
import promotionBg from "../resources/images/promotion-5.png";

const CBUBanner3 = (props) => {
    
    return (
        <div>
            <section className="banner">
                <div className="bg">
                    <img src={CBUBanner5Bg}/>
                    <div className="anima-arrow left"><img src={ArrowLeftImg} height="133" /></div>
                    <div className="anima-arrow right"><img src={ArrowRightImg} height="84" /></div>
                </div>
                <div className="caption">
                    <div className="content">
                        <div className='shortcut-menu'>
                            <div className='item'><a className='shortcut'>想繳費</a></div>
                            <div className='item'><a className='shortcut'>看帳單</a></div>
                            <div className='item'><a className='shortcut'>申請電子帳單</a></div>
                            <div className='item'><a className='shortcut'>查交易紀錄</a></div>
                            <div className='item'><a className='shortcut'>查上網用量</a></div>
                        </div>
                    </div>
                </div>
            </section>
            <section className={`promotion`} style={{transitionDelay: !props.isScrolling ? '2s': '0s'}} data-index={props.index}>
                <img src={promotionBg} />
            </section>
        </div>
    )
}

export default CBUBanner3;